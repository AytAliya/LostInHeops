package heops;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.*;

public class GamePanel extends JPanel implements KeyListener {
	private JFrame parent;
	private GameController controller;
	public GameController getController() {
		return controller;
	}

	public void setController(GameController controller) {
		this.controller = controller;
	}

	private Map map;
	private Character character;
	private Light light;
	private Indicators indicators;
	private Enemy[] enemies;
	private GameTimer gameTimer;
	private boolean gameRunning = false;
	private boolean caught;
	private boolean gameOver = false;
	private int life;
	private AudioClip sound;
	
	public GamePanel(JFrame parent, GameController controller){
		this.controller = controller;
		this.parent = parent;
		gameTimer = new GameTimer(this);
		addKeyListener(this);
	}
	
	public void startGame(Level level){
		gameTimer.clearHandlers();
		map = level.getMap();
		map.loadMap();
		character = level.getCharacter();
		light = level.getLight();
		indicators = level.getIndicators();
		gameTimer.addHandler(indicators);
		enemies = level.getEnemies();
		for(int i = 0; i < enemies.length; i++)
			gameTimer.addHandler(enemies[i]);
		setPreferredSize(new Dimension(map.getWidth(), map.getHeight()));
		setGameRunning(true);
		try{
			sound = Applet.newAudioClip(new File("src/resources/01.wav").toURI().toURL());
			sound.play();
		}
		catch(Exception e){e.printStackTrace();}
	}
	
	public Map getMap(){
		return map;
	}
	
	public Character getCharacter(){
		return character;
	}
	
	public Light getLight(){
		return light;
	}
	
	private int[][] getCorners(int x, int y){
		double xx = x;
		double yy = y;
		int tx = xx % Character.CHAR_WIDTH == 0 ? 1 : 0;
		int ty = yy % Character.CHAR_HEIGHT == 0 ? 1 : 0;
		
		int[][] xy = new int[4][2];
		xy[0][0] = tx + (int)Math.ceil(xx/Character.CHAR_WIDTH);	//tlx
		xy[0][1] = ty + (int)Math.ceil(yy/Character.CHAR_HEIGHT);	//tly
		xy[1][0] = xy[0][0]+Math.abs(tx-1);					//trx
		xy[1][1] = xy[0][1];								//try
		xy[2][0] = xy[0][0];								//blx
		xy[2][1] = xy[0][1]+Math.abs(ty-1);					//bly
		xy[3][0] = xy[0][0]+Math.abs(tx-1);					//brx
		xy[3][1] = xy[0][1]+Math.abs(ty-1);					//bry
		return xy;
	}
	
	public boolean onExit(int x, int y){
		int[][] xy = getCorners(x, y);
		
		for(int i = 0; i < 4; i++){
			int cx = xy[i][0];
			int cy = xy[i][1];
			if(map.getMapItem(cx-1, cy-1) == Map.MapItem.DOOR) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean canWalk(int x, int y){
		int[][] xy = getCorners(x, y);
		
		for(int i = 0; i < 4; i++){
			int cx = xy[i][0];
			int cy = xy[i][1];
			if(map.getMapItem(cx-1, cy-1) == Map.MapItem.WALL) {
				return false;
			}
		}
		
		return true;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		setBackground(Color.BLACK);
		map.draw(g);
		character.draw(g);
		for(int i = 0; i < enemies.length; i++)
			enemies[i].draw(g);
		light.draw(g);
		indicators.draw(g);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(!gameRunning) return;
		character.move(e);
		repaint();
	}

	public boolean isGameRunning() {
		return gameRunning;
	}

	public void setGameRunning(boolean gameRunning) {
		this.gameRunning = gameRunning;
		if(!gameRunning){
			gameTimer.stop();
		}
		else{
			gameTimer.start();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}

	public boolean isCaught() {
		return caught;
	}

	public void setCaught(boolean caught) {
		this.caught = caught;
		if(life == 1)
			this.gameOver = true;
		
		if(caught){
			setGameRunning(false);
			Timer timer = new Timer(2500, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setCaught(false);
					life--;
					if(isGameOver()){
						setGameOver(false);
						controller.loadStartScreen();
					}
					else {
						controller.loadCurrentLevel();
					}
				}
			});
			timer.setRepeats(false);
			timer.start();
		}
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}
}
