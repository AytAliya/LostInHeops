package heops;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Arc2D;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class Light {
	public GamePanel parent;
	
	private int factor = 3;
	private float lightAlpha = 0f;
	private Rectangle2D.Double screenRect;
	private Ellipse2D.Double visibleEllipse;
	private boolean enabled;
	
	public Rectangle getVisibleArea(){
		return visibleEllipse.getBounds();
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Coord getVisibleStartCoord(){
		int dx = ((factor - 1)/2 + 1)*Map.CELL_WIDTH;
		int dy = ((factor - 1)/2 + 1)*Map.CELL_HEIGHT;
		double x = parent.getCharacter().getX() - dx;
		double y = parent.getCharacter().getY() - dy;
		if(x < 0) x = 0;
		else x = Math.floor(x/Map.CELL_WIDTH);
		if(y < 0) y = 0;
		else y = Math.floor(x/Map.CELL_HEIGHT);
		return new Coord((int)x, (int)y);
	}
	
	public Coord getVisibleEndCoord(){
		int dx = Character.CHAR_WIDTH + ((factor - 1)/2 + 1)*Map.CELL_WIDTH;
		int dy = Character.CHAR_HEIGHT + ((factor - 1)/2 + 1)*Map.CELL_HEIGHT;
		int maxX = parent.getMap().getWidth();
		int maxY = parent.getMap().getHeight();
		double x = parent.getCharacter().getX() + dx;
		double y = parent.getCharacter().getY() + dy;
		if(x >= maxX) x = parent.getMap().getDimX()-1;
		else x = Math.floor(x/Map.CELL_WIDTH);
		if(y >= maxY) y = parent.getMap().getDimY()-1;
		else y = Math.floor(y/Map.CELL_HEIGHT);
		return new Coord((int)x, (int)y);
	}
	
	public Light(GamePanel parent){
		this.parent = parent;
		screenRect = new Rectangle2D.Double();
		visibleEllipse = new Ellipse2D.Double();
	}
	
	public void draw(Graphics g){
		if(!enabled)
			return;
		
		Graphics2D g2 = (Graphics2D)g;
		g2.setColor(Color.BLACK);
		int x = parent.getCharacter().getX()-((factor-1)/2)*Character.CHAR_WIDTH;
		int y = parent.getCharacter().getY()-((factor-1)/2)*Character.CHAR_HEIGHT;
		screenRect.setFrame(0, 0, parent.getWidth(), parent.getHeight());
		visibleEllipse.setFrame(x, y, Character.CHAR_WIDTH*factor, Character.CHAR_HEIGHT*factor);
		Area a = new Area(visibleEllipse);
		Area b = new Area(screenRect);
		b.subtract(a);
		g2.fill(b);
		g2.setColor(new Color(0f, 0f, 0f, lightAlpha));
		g2.fill(new Area(new Rectangle2D.Double(x, y, Character.CHAR_WIDTH*factor, Character.CHAR_HEIGHT*factor)));
	}
	
	public int getFactor() {
		return factor;
	}

	public void setFactor(int factor) {
		this.factor = factor;
	}

	public float decrementLight() {
		if(lightAlpha >= 1.0f)
			return lightAlpha;
		lightAlpha+=0.1;
		parent.repaint();
		return lightAlpha;
	}
	public float incrementLight() {
		if(lightAlpha <= 0.0f)
			return lightAlpha;
		lightAlpha-=0.1;
		parent.repaint();
		return lightAlpha;
	}
}
