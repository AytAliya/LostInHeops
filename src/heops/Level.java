package heops;

public class Level {
	private Map map;
	private Light light;
	private Indicators indicators;
	private Character character;
	private Enemy enemies[];
	public Map getMap() {
		return map;
	}
	public void setMap(Map map) {
		this.map = map;
	}
	public Light getLight() {
		return light;
	}
	public void setLight(Light light) {
		this.light = light;
	}
	public Indicators getIndicators() {
		return indicators;
	}
	public void setIndicators(Indicators indicators) {
		this.indicators = indicators;
	}
	public Character getCharacter() {
		return character;
	}
	public void setCharacter(Character character) {
		this.character = character;
	}
	public Enemy[] getEnemies() {
		return enemies;
	}
	public void setEnemies(Enemy[] enemies) {
		this.enemies = enemies;
	}
}
