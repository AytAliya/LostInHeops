package heops;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.Timer;

public class Indicators implements ITimeoutHandler {
	private GamePanel parent;
	private ImageIcon head;
	private int energyStatus;
	private int energyLength;
	private int speed = 500;
	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	private boolean isEnergySafe;
	public boolean isEnergySafe() {
		return isEnergySafe;
	}

	public void setEnergySafe(boolean isEnergySafe) {
		this.isEnergySafe = isEnergySafe;
	}

	public Indicators(GamePanel parent, boolean isEnergySafe){
		this.parent = parent;
		this.isEnergySafe = isEnergySafe;
		energyStatus = 255;
		energyLength = 50;
		head = new ImageIcon("src/resources/head.png");
	}
	
	public void draw(Graphics g){
		g.setColor(Color.WHITE);
		g.drawString("Light", parent.getWidth()-100, 20);
		g.setColor(new Color(255-energyStatus, energyStatus, 0));
		g.fillRect(parent.getWidth()-60, 5, energyLength, 20);
		g.drawRect(parent.getWidth()-60, 5, 50, 20);
		
		g.setColor(Color.WHITE);
		g.drawString("Lives", 10, 20);
		for(int i = 0; i < parent.getLife(); i++){
			g.drawImage(head.getImage(), 50+i*30, 5, 20, 20, null);
		}
		
		if(parent.isCaught()){
			Font font = g.getFont();
			g.setFont(new Font("Arial", Font.BOLD, 36));
			if(parent.isGameOver())
				g.drawString("GAME OVER", parent.getWidth()/2-100, parent.getHeight()/2);
			else if(energyStatus == 0){
				g.drawString("Out of light", parent.getWidth()/2-115, parent.getHeight()/2);
			}
			else g.drawString("You've been caught", parent.getWidth()/2-150, parent.getHeight()/2);
			
			
			g.setFont(font);
		}
	}
	
	@Override
	public void onTimeout(int tick) {
		if(tick%speed != 0)
			return;
		
		if(this.parent.getLight().isEnabled() && isEnergySafe){
			parent.getLight().decrementLight();
			energyStatus -= 30;
			if(energyStatus < 0) energyStatus = 0;
			energyLength -= 5;
			if(energyLength < 0) energyLength = 0;
		}
		if(energyStatus == 0)
			   parent.setCaught(true);
	}
}
