package heops;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class GameController {
	private Game parent;
	private GamePanel gamePanel;
	private StartScreen startScreen;
	private LoadScreen loadScreen;
	private AboutScreen aboutScreen;
	private int currentLevel = 0;
	private Timer timer;
	
	
	
	
	public GameController(Game parent){
		this.parent = parent;
		gamePanel = new GamePanel(this.parent, this);
		startScreen = new StartScreen();
		loadScreen = new LoadScreen();
		aboutScreen = new AboutScreen();
	}
	
	
	public void loadStartScreen(){
		parent.setPanel(startScreen);
		parent.pack();
		startScreen.grabFocus();
	}
	
	public void loadCurrentLevel(){
		loadLevel(currentLevel);
	}
	
	public void loadNextLevel(){
		currentLevel++;
		if(currentLevel == 5){
			loadStartScreen();
			JOptionPane.showMessageDialog(gamePanel, "Congratulations! You Won the Game!");
			return;
		}
		loadLevel(currentLevel);
	}
	
	public void loadLevel(final int index){
		loadScreen.setLevel(index+1);
		
		parent.setPanel(loadScreen);
		parent.pack();
		loadScreen.grabFocus();
		
	
			timer = new Timer(2000, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					gamePanel.startGame(getLevel(index));
					parent.setPanel(gamePanel);
					parent.pack();
					gamePanel.grabFocus();
				}
			});
			timer.setRepeats(false);
		
		timer.start();
	}
	
	public Level getLevel(int i){
		switch (i) {
		case 0:
			return getLevel0();
		case 1:
			return getLevel1();
		case 2:
			return getLevel2();
		case 3:
			return getLevel3();
		case 4:
			return getLevel4();
		}
		return getLevel0();
	}
	
	
	
	public Level getLevel0(){
		Level level = new Level();
		Map map = new Map(gamePanel, "src/resources/map2.txt");
		Character character = new Character(gamePanel, "src/resources/char.png");
		character.setStep(20);
		character.setX(Map.CELL_WIDTH);
		character.setY(Map.CELL_HEIGHT);
		Light light = new Light(gamePanel);
		light.setEnabled(true);
		light.setFactor(7);
		Indicators indicators = new Indicators(gamePanel, true);
		Enemy enemies[] = new Enemy[1];
		enemies[0] = new Enemy(gamePanel, 10*Map.CELL_WIDTH, 10*Map.CELL_HEIGHT, 8, 100, "src/resources/en1.png");
		level.setMap(map);
		level.setCharacter(character);
		level.setLight(light);
		level.setIndicators(indicators);
		level.setEnemies(enemies);
		
		return level;
	}
	
	public Level getLevel1(){
		Level level = new Level();
		Map map = new Map(gamePanel, "src/resources/map3.txt");
		Character character = new Character(gamePanel, "src/resources/char.png");
		character.setX(Map.CELL_WIDTH);
		character.setY(Map.CELL_HEIGHT);
		Light light = new Light(gamePanel);
		light.setEnabled(true);
		light.setFactor(6);
		Indicators indicators = new Indicators(gamePanel, true);
		Enemy enemies[] = new Enemy[2];
		enemies[0] = new Enemy(gamePanel, 8*Map.CELL_WIDTH, 2*Map.CELL_HEIGHT, 6, 300, "src/resources/en2.png");
		enemies[1] = new Enemy(gamePanel, 11*Map.CELL_WIDTH, 6*Map.CELL_HEIGHT, 6, 300, "src/resources/en2.png");
		level.setMap(map);
		level.setCharacter(character);
		level.setLight(light);
		level.setIndicators(indicators);
		level.setEnemies(enemies);
		
		return level;
	}
	
	public Level getLevel2(){
		Level level = new Level();
		Map map = new Map(gamePanel, "src/resources/map4.txt");
		Character character = new Character(gamePanel, "src/resources/char.png");
		character.setX(Map.CELL_WIDTH);
		character.setY(Map.CELL_HEIGHT);
		Light light = new Light(gamePanel);
		light.setEnabled(true);
		light.setFactor(5);
		Indicators indicators = new Indicators(gamePanel, true);
		Enemy enemies[] = new Enemy[2];
		enemies[0] = new Enemy(gamePanel, 10*Map.CELL_WIDTH, 10*Map.CELL_HEIGHT, 5, 350, "src/resources/en3.png");
		enemies[1] = new Enemy(gamePanel, 10*Map.CELL_WIDTH, 10*Map.CELL_HEIGHT, 5, 350, "src/resources/en3.png");
		level.setMap(map);
		level.setCharacter(character);
		level.setLight(light);
		level.setIndicators(indicators);
		level.setEnemies(enemies);
		
		return level;
	}
	
	public Level getLevel3(){
		Level level = new Level();
		Map map = new Map(gamePanel, "src/resources/map5.txt");
		Character character = new Character(gamePanel, "src/resources/char.png");
		character.setX(Map.CELL_WIDTH);
		character.setY(Map.CELL_HEIGHT);
		Light light = new Light(gamePanel);
		light.setEnabled(true);
		light.setFactor(4);
		Indicators indicators = new Indicators(gamePanel, true);
		Enemy enemies[] = new Enemy[2];
		enemies[0] = new Enemy(gamePanel, 10*Map.CELL_WIDTH, 10*Map.CELL_HEIGHT, 5, 400, "src/resources/en4.png");
		enemies[1] = new Enemy(gamePanel, 10*Map.CELL_WIDTH, 10*Map.CELL_HEIGHT, 5, 400, "src/resources/en4.png");
		level.setMap(map);
		level.setCharacter(character);
		level.setLight(light);
		level.setIndicators(indicators);
		level.setEnemies(enemies);
		
		return level;
	}
	
	public Level getLevel4(){
		Level level = new Level();
		Map map = new Map(gamePanel, "src/resources/map6.txt");
		Character character = new Character(gamePanel, "src/resources/char.png");
		character.setX(Map.CELL_WIDTH);
		character.setY(Map.CELL_HEIGHT);
		Light light = new Light(gamePanel);
		light.setEnabled(true);
		light.setFactor(3);
		Indicators indicators = new Indicators(gamePanel, true);
		Enemy enemies[] = new Enemy[1];
		enemies[0] = new Enemy(gamePanel, 10*Map.CELL_WIDTH, 10*Map.CELL_HEIGHT, 3, 500, "src/resources/en5.png");
		level.setMap(map);
		level.setCharacter(character);
		level.setLight(light);
		level.setIndicators(indicators);
		level.setEnemies(enemies);
		
		return level;
	}
	
	public JPanel getStartScreen(){
		return startScreen;
	}
	
	private class AboutScreen extends JPanel implements KeyListener{
		private Image about;
		
		public AboutScreen(){
			try {
				about = ImageIO.read(new File("src/resources/about.png"));
			} catch (IOException e) {
				JOptionPane.showMessageDialog(this, "Can not read the file");
			}
			setPreferredSize(new Dimension(700, 506));
			addKeyListener(this);
		}
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.drawImage(about, 0, 0, 700, 506, this);
		}
		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void keyPressed(KeyEvent e) {
			if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
				loadStartScreen();
			}
			
		}
		@Override
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}
	}
	
	private class LoadScreen extends JPanel {
		private int level;
		
		public LoadScreen(){
			setPreferredSize(new Dimension(700, 500));
			setBackground(Color.BLACK);
		}
		
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setColor(Color.WHITE);
			g.drawString("Level "+level, getWidth()/2, getHeight()/2);
		}

		public int getLevel() {
			return level;
		}

		public void setLevel(int level) {
			this.level = level;
		}
	}
	
	
	
	private class StartScreen extends JPanel implements KeyListener {
		private Image screen;
		private Image choice;
		private int state = 1;
		
		public StartScreen(){
			try {
				screen = ImageIO.read(new File("src/resources/start.png"));
				choice = ImageIO.read(new File("src/resources/choice.png"));
			} catch (IOException e) {
				JOptionPane.showMessageDialog(this, "Can not read the file");
			}
			setPreferredSize(new Dimension(700, 506));
			addKeyListener(this);
		}
		
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.drawImage(screen, 0, 0, 700, 506, this);
			
			switch(state){
			case 1:
				g.drawImage(choice, 220, 235, 20, 20, this);
				break;
			case 2:
				g.drawImage(choice, 220, 325, 20, 20, this);
				break;
			case 3:
				g.drawImage(choice, 220, 415, 20, 20, this);
				break;
			}
		}

		@Override
		public void keyTyped(KeyEvent e) {}
		
		@Override
		public void keyReleased(KeyEvent e) {}

		@Override
		public void keyPressed(KeyEvent e) {
			int keyCode = e.getKeyCode();
			
			switch (keyCode) {
			case KeyEvent.VK_DOWN:
				if(state < 3)state++;
				break;
			case KeyEvent.VK_UP:
				if(state > 1)state--;
				break;
			case KeyEvent.VK_ENTER:
				process();
				break;
			}
			
			repaint();
		}
		
		public void process(){
			if(state == 1){
				currentLevel = 0;
				gamePanel.setLife(3);
				loadLevel(currentLevel);
			}
			else if(state == 2){
				parent.setPanel(aboutScreen);
				parent.pack();
				aboutScreen.grabFocus();
			}
			else if(state == 3){
				System.exit(0);
			}
		}
	}
}
