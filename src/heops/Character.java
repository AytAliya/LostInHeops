package heops;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Character {
	private GamePanel parent;
	private Image charImg;
	public static final int CHAR_WIDTH = 40;
	public static final int CHAR_HEIGHT = 40;
	
	private int X = 0, Y = 0, SX = 0, SY = 0, step = 10;
	
	public Character(GamePanel parent, String charImgFile){
		this.parent = parent;
		try {
			charImg = ImageIO.read(new File(charImgFile));
		} catch (IOException e) {
			JOptionPane.showMessageDialog(parent, "Can not read the file");
		}
	}
	
	public int getX() {
		return X;
	}

	public void setX(int x) {
		X = x;
	}

	public int getY() {
		return Y;
	}

	public void setY(int y) {
		Y = y;
	}

	public void move(KeyEvent e){
		int dx = 0;
		int dy = 0;
		int sdx = 0;
		int sdy = 0;
		if(e.getKeyCode() == KeyEvent.VK_UP){			
			dy-=step;
			sdx=(SX+CHAR_WIDTH)%(CHAR_WIDTH*3);
			sdy=CHAR_HEIGHT*2;
		} else if(e.getKeyCode() == KeyEvent.VK_DOWN){
			dy+=step;
			sdx=(SX+CHAR_WIDTH)%(CHAR_WIDTH*3);
			sdy=0;
		} else if(e.getKeyCode() == KeyEvent.VK_LEFT){
			dx-=step;
			sdx=(SX+CHAR_WIDTH)%(CHAR_WIDTH*3);
			sdy=CHAR_HEIGHT*3;
		} else if(e.getKeyCode() == KeyEvent.VK_RIGHT){
			dx+=step;
			sdx=(SX+CHAR_WIDTH)%(CHAR_WIDTH*3);
			sdy=CHAR_HEIGHT;
	    }
		if(parent.canWalk(X+dx, Y+dy)){
			X+=dx;
			Y+=dy;
			SX = sdx;
			SY = sdy;
		}
		
		if(parent.onExit(X, Y)){
			parent.setGameRunning(false);
			parent.getController().loadNextLevel();
		}
	}
	
	public Rectangle2D.Double getBounds(){
		return new Rectangle2D.Double(X, Y, CHAR_WIDTH, CHAR_HEIGHT);
	}
	
	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

	public void draw(Graphics g){
		g.drawImage(charImg, X, Y, X+40, Y+40, SX, SY, SX+40, SY+40, this.parent);
	}
}
