package heops;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Timer;

public class GameTimer implements ActionListener{
	private Timer timer;
	private int interval = 10;
	private int tick = 0;
	private GamePanel parent;
	private ArrayList<ITimeoutHandler> handlers;
	
	public GameTimer(GamePanel parent){
		this.parent = parent;
		timer = new Timer(interval, this);
		handlers = new ArrayList<ITimeoutHandler>();
	}
	
	public void addHandler(ITimeoutHandler handler){
		handlers.add(handler);
	}
	
	public void start(){
		if(!timer.isRunning()) {
			timer.start();
		}
	}
	
	public void stop(){
		timer.stop();
	}
	
	public void clearHandlers(){
		handlers.clear();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		for (ITimeoutHandler handler : handlers) {
			handler.onTimeout(tick);
		}
		tick++;
		if(tick == Integer.MAX_VALUE) tick = 0;
		parent.repaint();
	}
}
