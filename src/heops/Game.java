package heops;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;

import javax.swing.*;


public class Game extends JFrame{
	
	private JPanel panel; 
	
	public static void main(String args[]){
		Game frame = new Game();
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		GameController gc = new GameController(frame);
		
		
		frame.setPanel(gc.getStartScreen());
		frame.pack();
		frame.setVisible(true);
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		if(this.panel != null) {
			this.setFocusable(false);
			this.remove(this.panel);
		}
		this.panel = panel;
		this.panel.setFocusable(true);
		this.add(this.panel);
		this.repaint();
	}
}
