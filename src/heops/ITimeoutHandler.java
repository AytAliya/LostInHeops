package heops;

public interface ITimeoutHandler {
	public void onTimeout(int tick);
}
