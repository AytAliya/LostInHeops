package heops;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class Enemy implements ITimeoutHandler{
	private GamePanel parent;
	private int X,Y;
	private int size = 40;
	private int speed;
	private int vision;
	private boolean botMode = true;
	private boolean near = false;
	private int direction = 0;
	private Image image;
	private String file;
	
	public int getX() {
		return X;
	}

	public void setX(int x) {
		X = x;
	}

	public int getY() {
		return Y;
	}

	public void setY(int y) {
		Y = y;
	}

	public Enemy(GamePanel parent, int x, int y, int speed, int vision, String file){
		this.parent = parent;
		this.file = file;
		try {
			image = ImageIO.read(new File(file));
		} catch (IOException e) {
			JOptionPane.showMessageDialog(parent, "Can not read the file");
		} 
		X = x;
		Y = y;
		this.speed = speed;
		this.vision = vision;
	}
	
	public void draw(Graphics g){
		boolean draw = false;
		if(parent.getLight().isEnabled()){
			if(parent.getLight().getVisibleArea().contains(new Point(X, Y))){
				draw = true;
			}
		}
		else{
			draw = true;
		}
		
		if(draw){
			g.drawImage(image, X, Y, this.parent);
		}
	}

	@Override
	public void onTimeout(int tick) {
		if(tick%speed != 0)
			return;
		Character character = parent.getCharacter();
		int diffx = Math.abs(character.getX()-X);
		int diffy = Math.abs(character.getY()-Y);
		
		if(diffx < vision && diffy < vision && !near) botMode = false;
		else if(diffx > vision || diffy > vision) near = false;
		
		int dx = 0;
		int dy = 0;
		if(botMode){
			switch (direction) {
			case 0:
				dx+=5;
				break;
			case 1:
				dx-=5;
				break;
			case 2:
				dy+=5;
				break;
			case 3:
				dy-=5;
				break;
			default:
				break;
			}
		}
		else {
			if(X < character.getX())dx+=5;
			else if(X > character.getX())dx-=5;
			
			if(Y < character.getY())dy+=5;
			else if(Y > character.getY())dy-=5;
		}
		
		if(dx != 0 && parent.canWalk(X+dx, Y))X+=dx;
		else if(dy != 0 && parent.canWalk(X, Y+dy))Y+=dy;
		else if(botMode){
			direction = new Random().nextInt(4);
		}
		else {
			botMode = true;
			near = true;
		}
		
		Rectangle2D.Double r = new Rectangle2D.Double(X, Y, size, size);
		if(r.intersects(parent.getCharacter().getBounds())){
			parent.setCaught(true);
		}
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getVision() {
		return vision;
	}

	public void setVision(int vision) {
		this.vision = vision;
	}
}
