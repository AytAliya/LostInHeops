package heops;

import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Map {
	public static final int CELL_WIDTH = 40;
    public static final int CELL_HEIGHT = 40;
	
    private int dimX = 0;
    private int dimY = 0;
    
    private String mapFile;
    
    public MapItem getMapItem(int x, int y){
    	if(x < 0 || x >= dimX || y < 0 || y >= dimY)
    		return MapItem.WALL;
    	return map[y][x];
    }
    
    public int getWidth(){
    	return dimX*CELL_WIDTH;
    }
    
    public int getHeight(){
    	return dimY*CELL_HEIGHT;
    }
    
    public int getDimY() {
		return dimY;
	}

	public void setDimY(int dimY) {
		this.dimY = dimY;
	}

	public int getDimX() {
		return dimX;
	}

	public void setDimX(int dimX) {
		this.dimX = dimX;
	}
    
	private GamePanel parent;
	
	public enum MapItem {
		EMPTY, WALL, DOOR, BATTERY
	}
	
	private MapItem[][] map;
	private ImageIcon wall;
	private ImageIcon empty;
	private ImageIcon door;
	private ImageIcon battery;
	
	public Map(GamePanel parent, String mapFile){
		this.parent = parent;
		this.mapFile = mapFile;
		wall = new ImageIcon("src/resources/wall.png");
		empty = new ImageIcon("src/resources/empty.png");
		door = new ImageIcon("src/resources/door.png");
		battery = new ImageIcon("src/resources/blue.png");
	}
	
	public void draw(Graphics g){
		Coord start = new Coord(0, 0);
		Coord end = new Coord(dimX-1, dimY-1);
		if(parent.getLight().isEnabled()){
			start = parent.getLight().getVisibleStartCoord();
			end = parent.getLight().getVisibleEndCoord();
		}
		
		for (int i = start.getY(); i <= end.getY(); i++) {
			for (int j = start.getX(); j <= end.getX(); j++) {
				MapItem item = map[i][j];
				switch(item){
					case EMPTY:
						g.drawImage(empty.getImage(), j * CELL_WIDTH, i * CELL_HEIGHT, null);
						break;
					case WALL:
						g.drawImage(wall.getImage(), j * CELL_WIDTH, i * CELL_HEIGHT, null);
						break;
					case DOOR:
						g.drawImage(door.getImage(), j * CELL_WIDTH, i * CELL_HEIGHT, null);
						break;
					case BATTERY:
						g.drawImage(battery.getImage(), j * CELL_WIDTH, i * CELL_HEIGHT, null);
						break;
					default:
						g.drawImage(empty.getImage(), j * CELL_WIDTH, i * CELL_HEIGHT, null);
				}
            }
        }
	}
	
	public void loadMap() {
		Scanner in = null;
		try {
			in = new Scanner(new File(mapFile));
			dimY = in.nextInt();
			dimX = in.nextInt();
			map = new MapItem[dimY][dimX];
			
			for (int i = 0; i < dimY; i++) {
				for (int j = 0; j < dimX; j++) {
					int x = in.nextInt();
					switch (x) {
                		case 0:
                			map[i][j] = MapItem.EMPTY;
                			break;
                		case 1:
                			map[i][j] = MapItem.WALL;
                			break;
                		case 2:
                			map[i][j] = MapItem.DOOR;
                			break;
						case 3:
							map[i][j] = MapItem.BATTERY;
							break;
                		default:
                			map[i][j] = MapItem.EMPTY;
					}
				}
			}
        } catch(FileNotFoundException e) {
        	JOptionPane.showMessageDialog(parent, "Can not read the file");
        }
		finally {
			if(in != null)
				in.close();
		}
	}
}
